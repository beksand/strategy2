package Strategy;

public class LowSetting implements IGraphicsSetting {
    @Override
    public int getNeededProcessingPower() {
        return 100;
    }

    @Override
    public void processFrame() {

    }
}
