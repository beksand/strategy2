package Strategy;

public interface IGraphicsSetting {
    public int getNeededProcessingPower();
    public void processFrame();
}
