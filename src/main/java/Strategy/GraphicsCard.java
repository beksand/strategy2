package Strategy;

public class GraphicsCard {
    private IGraphicsSetting graphicsset;

    public IGraphicsSetting getGraphicsset() {
        return graphicsset;
    }

    public void setGraphicsset(IGraphicsSetting graphicsset) {
        this.graphicsset = graphicsset;
    }

    public int graphicStrategy(){
      return graphicsset.getNeededProcessingPower();
    }
}
