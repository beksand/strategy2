package Strategy;

public class HDSetting implements IGraphicsSetting {
    @Override
    public int getNeededProcessingPower() {
        return 1000;
    }

    @Override
    public void processFrame() {

    }
}
