package Strategy;

public class MediumSetting implements IGraphicsSetting {
    @Override
    public int getNeededProcessingPower() {
        return 500;
    }

    @Override
    public void processFrame() {

    }
}
