package Strategy;

//Pracuj z gitem. Przed każdymi liniami wykonaj commit'a.
//
//        Celem tego zadania nie jest skupianie się na szczegółach działania (implementacji metod), tylko na własnej
// implementacji wzorca projektowego.
//        Stwórz klasę GraphicsCard która reprezentuje kartę graficzną. Strategia w tym projekcie będzie służyć do
// podmieniania w tej klasie obiektu sterownika. Stwórz interfejs sterownika/ustawień karty graficznej. Mechanizm
// strategii powinien być wykorzystany do podmiany w klasie GraphicsCard obiektu sterownika/ustawienia karty graficznej.
//        Sterownik powinien posiadać metodę:
//        - getNeededProcessingPower która zwraca 1000 dla werji HDSetting, 500 dla MediumSetting, oraz 100 dla LowSetting.
//        - ***processFrame która jako parametr przyjmuje ramkę (tablica dwuwymiarowa int'ów) i w zależności od typu
// sterownika (klasy dziedziczącej) wypełnia tablicę innymi wartościami.
//        Podane powyżej dane powinny wystarczyć do stworzenia modelu w oparciu o wzorzec projektowy strategia. Poniżej
// znajdują się wskazówki do wykonania tego zadania.
//        1.) Stwórz klasę GraphicsCard.
//        2.) Stwórz klasę IGraphicsSetting
//        3.) Stwórz w GraphicsCard pole typu IGraphicsSetting (strategia).
//        4.) Stwórz 3 klasy dziedziczące: HDSetting, MediumSetting, LowSetting które dziedziczą po IGraphicsSetting(strategia)
//        5.) Dodaj do klasy IGraphicsSetting oraz klas dziedziczących implementację metod getNeededProcessingPower i ***processFrame.
//        6.) Stwórz implementację testową (main) która pozwala przestawiać podmianę klas i zasadę działania.
//        7.) Stwórz dodatkowe ustawienie (własne)

public class Main {
    public static void main(String[] args) {
        GraphicsCard card = new GraphicsCard();
        card.setGraphicsset(new LowSetting());
        System.out.println(card.graphicStrategy());

        card.setGraphicsset(new MediumSetting());
        System.out.println(card.graphicStrategy());

        card.setGraphicsset(new HDSetting());
        System.out.println(card.graphicStrategy());
    }
}
